# pixel-icons
A little icon pack with pixel-perfect icons I made for myself.
Most of icon design is done in [Figma](https://www.figma.com/file/lvuobMQXyCk1C69x7TyxtF/Pixel-icons)
Icon colors are based on [Gruvbox theme](https://github.com/morhetz/gruvbox), but if needed
they can be changed to match colors for another theme.